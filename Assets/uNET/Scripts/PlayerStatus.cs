﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

namespace uNET{
public class PlayerStatus :  NetworkBehaviour{

	[SerializeField,SyncVar] float m_HP;
	[SerializeField] Text HPText;
	[SerializeField] GameObject DeadUI; 
	bool isLive = true;
	FirstPersonController FPSCtrl;

	void Awake(){
		FPSCtrl = GetComponent<FirstPersonController> ();
	}

	void Start(){
		Transform canvas = GameObject.Find ("PlayerCanvas").transform;
		HPText = canvas.FindChild ("Text (1)").GetComponent<Text> ();
		DeadUI = canvas.FindChild ("Text").gameObject;
		DeadUI.SetActive (false);
	}

	void Update(){

		if (!isLocalPlayer) {
			return;
		}

		if (m_HP <= 0 && isLive) {
			FPSCtrl.enabled = false;
			isLive = false;
			DeadUI.SetActive (true);
		}
		HPText.text = m_HP.ToString ();
	}

	public void Damage(float _value){
		
		if (_value >= 0) {
			m_HP -= _value;
			if (m_HP <= 0) {
				m_HP = 0;
			}
		}

	}

}
}
