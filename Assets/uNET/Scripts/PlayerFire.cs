﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace uNET{
public class PlayerFire : NetworkBehaviour{

	[SerializeField] float m_Range;
	[SerializeField] float m_OffensivePower;
	[SerializeField] GameObject BulletPrefab;
	[SerializeField] GameObject MissilePrefab;
	[SerializeField] float m_ReloadTime;
	[SerializeField] Camera PlayerCamera;

	bool m_IsMissileFire = true;

	void Awake(){
		ClientScene.RegisterPrefab (BulletPrefab);
	}

	void Update(){

		if (!isLocalPlayer) {
			return;
		}

		if (Input.GetButtonDown("Fire1")) {
			Fire ();
		}
		if (Input.GetButtonDown ("Fire2") && m_IsMissileFire) {
			MissileFire ();
		}

	}


	void Fire(){
		CmdFire (PlayerCamera.transform.position, PlayerCamera.transform.forward);
	}

	[Command]
	void CmdFire(Vector3 position, Vector3 forward){

		GameObject bullet = Instantiate (BulletPrefab, position, Quaternion.identity) as GameObject;
		bullet.GetComponent<Rigidbody> ().velocity = forward * 100;
		NetworkServer.Spawn (bullet);
		bullet.AddComponent<ObjectDestroyerOnNetwork> ().DestroyOnNetwork (bullet, 5f);

		Ray ray = new Ray (position, forward);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, m_Range)) {
			try{
				PlayerStatus ps = hit.transform.GetComponent<PlayerStatus>();
				ps.Damage(m_OffensivePower);
			}catch(System.Exception e){
				
			}
		}

	}

	void MissileFire(){
		m_IsMissileFire = false;
		CmdMissileFire ();
		Invoke ("ResetIsMissileFire", m_ReloadTime);
	}

	[Command]
	void CmdMissileFire(){
		GameObject missile = Instantiate (MissilePrefab, transform.position + transform.forward * 2, transform.rotation) as GameObject;
		missile.AddComponent<ObjectDestroyerOnNetwork> ().DestroyOnNetwork (missile, 5f);
		NetworkServer.Spawn (missile);
	}

	void ResetIsMissileFire(){
		m_IsMissileFire = true;
	}
}
}