﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace uNET{
public class PlayerInfo : NetworkLobbyPlayer {

	[SerializeField] Button ReadyButton;
	[SerializeField] InputField PlayerInput;

	[SyncVar(hook = "UpdateUserName")] string m_PlayerName = string.Empty;

	public override void OnClientEnterLobby (){
		base.OnClientEnterLobby ();

		ReadyButton.onClick.AddListener (Ready);
		//ListManager.Instance.Add (this);

		if (isLocalPlayer) {
			ReadyButton.GetComponentInChildren<Text>().text = "Ready";
		} else {
			ReadyButton.GetComponentInChildren<Text>().text = "---";
		}

		if (m_PlayerName == string.Empty) {
			m_PlayerName = "Player" + ListManager.Instance.transform.childCount;
		}
		PlayerInput.text = m_PlayerName;
		//NameChange (m_PlayerName);
		ReadyButton.interactable = isLocalPlayer;
		PlayerInput.interactable = isLocalPlayer;
	}

	public override void OnStartAuthority (){
		base.OnStartAuthority ();

		ReadyButton.GetComponentInChildren<Text>().text = "Ready";
		ReadyButton.onClick.RemoveAllListeners ();
		ReadyButton.onClick.AddListener (Ready);
		ReadyButton.interactable = true;
		PlayerInput.interactable = true;
		PlayerInput.onEndEdit.AddListener (NameChange);
		Lobby.Instance.TryToAddPlayer ();

	}

	void Update(){

		if (ListManager.Instance.ListCount == 1) {
			ReadyButton.gameObject.SetActive (false);
		} else {
			ReadyButton.gameObject.SetActive (true);
		}

	}

	public override void OnClientReady (bool readyState){

		if (readyState) {
			ReadyButton.GetComponentInChildren<Text>().text = "Waiting";
			ReadyButton.onClick.RemoveAllListeners ();
			ReadyButton.onClick.AddListener (ReadyCancel);
		} else {
			ReadyButton.GetComponentInChildren<Text>().text = isLocalPlayer ? "Ready" : "----";
			ReadyButton.onClick.RemoveAllListeners ();
			ReadyButton.onClick.AddListener (Ready);
		}

	}

	public void Ready(){
		SendReadyToBeginMessage ();
	}

	public void ReadyCancel(){
		SendNotReadyToBeginMessage ();
	}

	public void UpdateUserName(string name){
		PlayerInput.text = name;
	}

	public void NameChange(string name){
		CmdNameChange (name);
	}

	[Command]
	public void CmdNameChange(string name){
		m_PlayerName = name;
	}

}
}
