﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace uNET{
public class ObjectDestroyerOnNetwork : NetworkBehaviour {

	GameObject DestoryGameObject;

	public void DestroyOnNetwork(GameObject _gameobject){
		DestoryGameObject = _gameobject;
		InvokeDestroy ();
	}

	public void DestroyOnNetwork(GameObject _gameobject,float time){
		DestoryGameObject = _gameobject;
		Invoke ("InvokeDestroy",time);
	}

	void InvokeDestroy(){
		Destroy (DestoryGameObject);
		NetworkServer.Destroy (DestoryGameObject);
	}

}
}
