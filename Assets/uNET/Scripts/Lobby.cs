﻿using System.Collections;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace uNET{
public class Lobby : NetworkLobbyManager {

	static public Lobby Instance;

	[SerializeField] GameObject EntrancePanel;
	[SerializeField] GameObject LobbyPanel;
	[SerializeField] Button CreateButton;
	[SerializeField] Button JoinButton;
	[SerializeField] InputField JoinInput;
	[SerializeField] Text AddressText;

	Canvas m_Canvas;
	string m_Address;

	void Awake(){
		if (FindObjectsOfType<Lobby> ().Length > 1) {
			//Destroy (gameObject);
		}
		m_Canvas = GetComponent<Canvas> ();
	}

	void Start(){
		Instance = this;

		DontDestroyOnLoad (gameObject);
	}

	void Update(){
		if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 0){
			string hostname = Dns.GetHostName ();
			IPAddress[] addressList = Dns.GetHostAddresses (hostname);
			foreach (IPAddress address in addressList) {
				m_Address = address.ToString ();
				AddressText.text = "Your Address:" + m_Address;
			}
		}
	}

	public void OnClickCreate(){
		EntrancePanel.SetActive (false);
		StartHost ();
		LobbyPanel.SetActive (true);
	}

	public override void OnStartHost ()
	{
		base.OnStartHost ();
		EntrancePanel.SetActive (false);
		LobbyPanel.SetActive (true);
	}

	public void OnClickJoin(){
		networkAddress = JoinInput.text;
		JoinInput.text = string.Empty;
		EntrancePanel.SetActive (false);
		StartClient ();
		LobbyPanel.SetActive (true);

	}

	public override GameObject OnLobbyServerCreateLobbyPlayer (NetworkConnection conn, short playerControllerId){
		GameObject obj = Instantiate (lobbyPlayerPrefab.gameObject) as GameObject;
		return obj;
	}

	public override void OnLobbyServerPlayersReady (){
		base.OnLobbyServerPlayersReady ();
	}

	public override bool OnLobbyServerSceneLoadedForPlayer (GameObject lobbyPlayer, GameObject gamePlayer){
		return base.OnLobbyServerSceneLoadedForPlayer (lobbyPlayer, gamePlayer);
	}

	public override void OnLobbyClientSceneChanged (NetworkConnection conn){
		m_Canvas.enabled = false;
		base.OnLobbyClientSceneChanged (conn);
	}

}
}