﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace uNET{
public class Missile : NetworkBehaviour {

	[SerializeField] float m_Speed;
	[SerializeField] float m_OffensivePpower;
	Rigidbody m_Rigidbody;

	// Use this for initialization
	void Start () {
		m_Rigidbody = GetComponent<Rigidbody> ();
		m_Rigidbody.velocity = transform.forward * m_Speed;
	}

	[ServerCallback]
	void OnCollisionEnter(Collision _other){
		try{
			PlayerStatus ps = _other.gameObject.GetComponent<PlayerStatus>();
			ps.Damage(m_OffensivePpower);
		}catch(System.Exception e){

		}

		gameObject.AddComponent<ObjectDestroyerOnNetwork> ().DestroyOnNetwork (gameObject);
	}
}
}
