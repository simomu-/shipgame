﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class ShipMovement : MonoBehaviour {
	
	[HideInInspector] public bool IsAccel = true;
	[HideInInspector] public bool IsRudder = true;

	[SerializeField] float m_ForwardAccelPower;
	[SerializeField,Range(0,1f)] float m_BackAccelMagnification;
	[SerializeField] float m_RudderTorlque;
	[SerializeField] AudioSource m_EngineAudio;
	//[SerializeField] Transform m_AccelTransform;
	//[SerializeField] Transform m_MeshTransform;
	Rigidbody m_Rigidbody;

	void Awake(){
		m_Rigidbody = GetComponent<Rigidbody> ();
	}

	void Update(){
		if (IsAccel) {	
			Accel ();
		}
		if (IsRudder) {
			Rudder ();
		}
	}

	void Accel(){
		float inputValue = CrossPlatformInputManager.GetAxis ("Vertical");
		if (inputValue < m_BackAccelMagnification * -1) {
			inputValue = m_BackAccelMagnification * -1;
		}
		m_Rigidbody.AddForce(transform.forward * m_ForwardAccelPower * inputValue * Time.deltaTime,ForceMode.Impulse);
		m_EngineAudio.volume = Mathf.Abs (inputValue * 0.1f);
	}

	void Rudder(){
		float speed = m_Rigidbody.velocity.magnitude;
		float inputValue = CrossPlatformInputManager.GetAxis ("Horizontal");
		float rotateValue = m_RudderTorlque * speed * inputValue * Time.deltaTime;
		Quaternion rotate = Quaternion.Euler (0, rotateValue, 0);
		m_Rigidbody.MoveRotation (m_Rigidbody.rotation * rotate);
		//Lean (rotateValue);
	}

	/*
	void Lean(float value){
		m_MeshTransform.localEulerAngles += new Vector3 (value, 0, 0);
	}
	*/

}
