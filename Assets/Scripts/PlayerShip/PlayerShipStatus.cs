﻿using UnityEngine;
using System.Collections;

public class PlayerShipStatus : Status {

	[SerializeField] ShipMovement m_ShipMovement;
	[SerializeField] ShipFire m_ShipFire;
	[SerializeField] float m_RestorationTime;
	[SerializeField,Range(0,1f)] float m_CatchFireProbability;
	[SerializeField,Range(0,1f)] float m_InundatedProbability;
	[SerializeField] GameObject[] Flames;
	/*
	bool m_IsAccel = true;
	bool m_IsRudder = true;
	bool m_IsFire = true;
	*/
	bool m_IsCathFire = false;
	bool m_IsInundated = false;

	public override void Update (){
		base.Update ();
		if (m_IsLive) {
			UIController.Instance.UpdateHealth (transform, NormalHealth, false);
		}
		UIController.Instance.UpdateMyHealth (NormalHealth);

		if (Input.GetButtonDown ("Cancel")) {
			UIController.Instance.SetActiveEscPanel (true);
		}

		m_ShipFire.IsAim = !Input.GetButton ("Fire2");
	}

	public override void Damage (float d, float magnification, string colliderName,WeaponType type){
		switch(colliderName){
		case "Engine":
			m_ShipMovement.IsAccel = false;
			Invoke ("IsAccelInvoke", m_RestorationTime);
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.EngineStop, true);
			break;
		case "Rudder":
			m_ShipMovement.IsRudder = false;
			Invoke ("IsRudderInvoke", m_RestorationTime);
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.RudderStop, true);
			break;
		}

		switch (type) {
		case WeaponType.Torpedo:
			if (Random.Range (0, 1f) <= m_InundatedProbability && !m_IsInundated) {
				StartCoroutine (InundatedCoroutine ());
			}
			break;
		case WeaponType.Shell:
			if (Random.Range (0, 1f) <= m_CatchFireProbability && !m_IsCathFire) {
				StartCoroutine (CatchFireCoroutine ());
				UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, true);
			}
			break;
		}

		if (m_IsLive) {
			m_Health -= d * magnification;
			if (m_Health <= 0) {
				m_Health = 0;
				OnHealthZero ();
			}
		}
	}

	public override void OnHealthZero (){
		base.OnHealthZero ();
		m_ShipMovement.IsAccel = false;
		m_ShipMovement.IsRudder = false;
		m_ShipFire.IsAim = false;
		m_ShipFire.IsFire = false;
		UIController.Instance.SetMessageText ("game over", Color.red, true);
		UIController.Instance.TimeCountStop ();
		GameManager.Instance.FinishSinglePlayGame (GameManager.Instance.DestroyedEnemy, UIController.Instance.ServiveTime);
		GameManager.Instance.SceneChange ("Title", 10f);
		GameManager.Instance.AudioVolume (false);
	}

	IEnumerator CatchFireCoroutine(){
		m_IsCathFire = true;
		UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, true);
		for (int i = 0; i < Flames.Length; i++) {
			Flames [i].SetActive (true);
		}
		for (float i = 0; i < m_RestorationTime; i += Time.deltaTime) {
			m_Health -= Time.deltaTime;
			if (m_Health <= 0 && m_IsLive) {
				OnHealthZero ();
				yield break;
			}
			yield return null;
		}
		//yield return new WaitForSeconds (m_RestorationTime);
		for (int i = 0; i < Flames.Length; i++) {
			Flames [i].SetActive (false);
		}
		UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, false);
		m_IsCathFire = false;
	}

	IEnumerator InundatedCoroutine(){
		m_IsInundated = true;
		UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, true);
		for (float i = 0; i < m_RestorationTime; i += Time.deltaTime) {
			m_Health -= Time.deltaTime;
			if (m_Health <= 0 && m_IsLive) {
				OnHealthZero ();
				yield break;
			}
			yield return null;
		}
		//yield return new WaitForSeconds (m_RestorationTime);
		UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, false);
		m_IsInundated = false;
	}

	void IsAccelInvoke(){
		if (m_IsLive) {
			m_ShipMovement.IsAccel = true;
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.EngineStop, false);
		}
	}

	void IsRudderInvoke(){
		if(m_IsLive){
			m_ShipMovement.IsRudder = true;
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.RudderStop, false);
		}
	}
}
