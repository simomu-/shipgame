﻿using UnityEngine;
using System.Collections;

public class RadarController : MonoBehaviour {

	public static RadarController Instance;
	[HideInInspector] public bool IsRadar = false;

	[SerializeField] Camera m_Camera;

	void Awake(){
		Instance = this;
	}

	void Update(){
		if (Input.GetKey (KeyCode.Tab)) {
			IsRadar = true;
		} else {
			IsRadar = false;
		}
		m_Camera.enabled = IsRadar;
	}

}
