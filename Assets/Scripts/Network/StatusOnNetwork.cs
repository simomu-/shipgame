﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class StatusOnNetwork : NetworkBehaviour {

	[SerializeField,SyncVar] protected float m_Health = 100f;
	[SerializeField] GameObject m_PositionUI;
	[SerializeField,SyncVar] protected string m_ShipName;
	[SerializeField,SyncVar] Color m_Color = Color.green;
	[SerializeField] GameObject m_ExplosionPrefab;
	[SerializeField] AudioSource[] m_ExplosionAudio;
	public float NormalHealth{
		get{
			return m_Health / 100f;
		}
	}
	protected bool m_IsLive = true;
	protected bool m_OnZeroHealthHappen = false;

	protected bool m_OnCamera = false;

	public virtual void Awake(){
		SetMyColor ();
	}

	public virtual void Start(){
		UIController.Instance.AddShip (transform,m_ShipName,m_Color);
		if (isLocalPlayer) {
			gameObject.tag = "Player";
		}
	}

	public virtual void Update(){
		FixTransform ();

		if (m_IsLive) {
			float distance = Vector3.Distance (Camera.main.transform.position, transform.position);
			if (m_OnCamera && distance <= 100f) {
				UIController.Instance.UpdateHealth (transform, NormalHealth, true);
			} else {
				UIController.Instance.UpdateHealth (transform, NormalHealth, false);
			}
			m_OnCamera = false;
		}
	}

	public void SetMyColor(){
		m_PositionUI.GetComponent<SpriteRenderer> ().color = m_Color;
	}

	public void FixTransform(){
		if (m_IsLive) {
			Vector3 newPosition = transform.position;
			newPosition.y = 0;
			transform.position = newPosition;
		}
	}

	public virtual void Damage(float d,float magnification,string colliderName,WeaponType type){
	}
		

	public virtual void OnHealthZero(){
		RpcOnHealthZero ();
	}
		
	public virtual void RpcOnHealthZero(){
		m_IsLive = false;
		m_PositionUI.SetActive (false);
		Rigidbody rigidbody = GetComponent<Rigidbody> ();
		rigidbody.constraints = RigidbodyConstraints.None;
		rigidbody.angularDrag = 10f;
		rigidbody.velocity -= transform.up * 0.5f;
		UIController.Instance.RemoveShip (transform);
		GameObject explosion = Instantiate (m_ExplosionPrefab,transform.position,Quaternion.identity) as GameObject;
		NetworkServer.Spawn (explosion);
		m_ExplosionAudio [(int)Random.Range (0, m_ExplosionAudio.Length)].Play ();
		//Destroy (gameObject, 10f);
		Invoke("DestroyInvoke",10f);
	}

	public void CollisionDamage(Vector3 relativeSpeed){
		float damage = relativeSpeed.magnitude * 20;
		if (m_IsLive) {
			m_Health -= damage;
			if (m_Health <= 0) {
				m_Health = 0;
				OnHealthZero ();
			}
		}
	}

	[ServerCallback]
	void OnCollisionEnter(Collision _other){
		StatusOnNetwork otherStatus = _other.gameObject.GetComponent<StatusOnNetwork> ();
		if (otherStatus != null && otherStatus.m_IsLive) {
			Vector3 damage = _other.relativeVelocity;
			_other.gameObject.GetComponent<StatusOnNetwork> ().CollisionDamage (damage);
		}

	}

	void OnWillRenderObject(){
		if (Camera.current.tag == "MainCamera") {
			m_OnCamera = true;
		}
	}

	void DestroyInvoke(){
		NetworkServer.Destroy (gameObject);
		Destroy (gameObject);
	}

}
