﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerStatuOnNetwork : NetworkBehaviour {


	[SerializeField,SyncVar] protected float m_Health = 100f;
	[SerializeField] GameObject m_PositionUI;
	[SerializeField] GameObject m_PlayerPositionUI;
	[SerializeField,SyncVar] public string m_ShipName;
	[SerializeField,SyncVar] public Color m_Color = Color.green;
	[SerializeField] GameObject m_ExplosionPrefab;
	[SerializeField] AudioSource[] m_ExplosionAudio;
	[Space]
	[SerializeField] ShipMovement m_ShipMovement;
	[SerializeField] FireOnNetwork m_ShipFire;
	[SerializeField] float m_RestorationTime;
	[SerializeField,Range(0,1f)] float m_CatchFireProbability;
	[SerializeField,Range(0,1f)] float m_InundatedProbability;
	[SerializeField] GameObject[] Flames;

	public float NormalHealth{
		get{
			return m_Health / 100f;
		}
	}
	[HideInInspector] public bool m_IsLive = true;
	bool m_OnZeroHealthHappen = false;

	bool m_OnCamera = false;

	bool m_IsCathFire = false;
	[SyncVar] bool m_IsInundated = false;
	[SyncVar] bool m_IsAccel = true;
	[SyncVar] bool m_IsRudder = true;

	void Awake(){
		
	}

	void Start(){
		MultiPlayManager.Instance.Add (this);
		SetMyColor ();
		UIController.Instance.AddShip (transform,m_ShipName,m_Color);
		if (isLocalPlayer) {
			gameObject.tag = "Player";
			m_PlayerPositionUI.SetActive (true);
		}
	}

	void Update (){

		FixTransform ();
		if (m_IsLive) {
			float distance = Vector3.Distance (Camera.main.transform.position, transform.position);
			if (m_OnCamera && distance <= 100f && !isLocalPlayer) {
				UIController.Instance.UpdateHealth (transform, NormalHealth, true);
			} else {
				UIController.Instance.UpdateHealth (transform, NormalHealth, false);
			}
			m_OnCamera = false;
		}
			
		if (isLocalPlayer) {
			if (m_IsLive) {
				UIController.Instance.UpdateHealth (transform, NormalHealth, false);
			}
			UIController.Instance.UpdateMyHealth (NormalHealth);
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.Inuntaded, m_IsInundated);
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.EngineStop, !m_IsAccel);
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.RudderStop, !m_IsRudder);
			m_ShipFire.IsAim = !Input.GetButton ("Fire2");
		}

	}

	void OnDestroy(){
		if (m_IsLive) {
			MultiPlayManager.Instance.Remove (this);
			MultiPlayManager.Instance.IsFinish = true;
			UIController.Instance.RemoveShip (transform);
		}
	}

	public void SetMyColor(){
		m_PositionUI.GetComponent<SpriteRenderer> ().color = m_Color;
	}

	public void FixTransform(){
		if (m_IsLive) {
			Vector3 newPosition = transform.position;
			newPosition.y = 0;
			transform.position = newPosition;
		}
	}

	public void Damage (float d, float magnification, string colliderName,WeaponType type){
		switch(colliderName){
		case "Engine":
			m_ShipMovement.IsAccel = false;
			m_IsAccel = false;
			Invoke ("IsAccelInvoke", m_RestorationTime);
			if (isLocalPlayer) {
				UIController.Instance.MyStatusAlment (UIController.StatusAlments.EngineStop, true);
			}
			break;
		case "Rudder":
			m_ShipMovement.IsRudder = false;
			m_IsRudder = false;
			Invoke ("IsRudderInvoke", m_RestorationTime);
			if (isLocalPlayer) {
				UIController.Instance.MyStatusAlment (UIController.StatusAlments.RudderStop, true);
			}
			break;
		}

		switch (type) {
		case WeaponType.Torpedo:
			if (Random.Range (0, 1f) <= m_InundatedProbability && !m_IsInundated) {
				StartCoroutine (InundatedCoroutine ());
			}
			break;
		case WeaponType.Shell:
			if (Random.Range (0, 1f) <= m_CatchFireProbability && !m_IsCathFire) {
				StartCoroutine (CatchFireCoroutine ());
				//UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, true);
			}
			break;
		}

		if (m_IsLive) {
			m_Health -= d * magnification;
			if (m_Health <= 0 ) {
				m_Health = 0;
				RpcOnHealthZero ();
			}
		}
	}
		
	/*
	public override void OnHealthZero (){
		base.OnHealthZero ();
	}*/
	[ClientRpc]
	public void RpcOnHealthZero (){

		m_IsLive = false;
		m_PositionUI.SetActive (false);
		Rigidbody rigidbody = GetComponent<Rigidbody> ();
		rigidbody.constraints = RigidbodyConstraints.None;
		rigidbody.angularDrag = 10f;
		rigidbody.velocity -= transform.up * 0.5f;
		UIController.Instance.RemoveShip (transform);
		//UIController.Instance.RemoveMultiPlayer (m_Color);
		MultiPlayManager.Instance.Remove(this);
		MultiPlayManager.Instance.IsFinish = true;
		GameObject explosion = Instantiate (m_ExplosionPrefab,transform.position,Quaternion.identity) as GameObject;
		//NetworkServer.Spawn (explosion);
		m_ExplosionAudio [(int)Random.Range (0, m_ExplosionAudio.Length)].Play ();
		//Destroy (gameObject, 10f);
		Invoke("DestroyInvoke",10f);

		m_ShipMovement.IsAccel = false;
		m_ShipMovement.IsRudder = false;
		m_ShipFire.IsAim = false;
		m_ShipFire.IsFire = false;
		if (isLocalPlayer) {
			UIController.Instance.SetMessageText ("game over", Color.red, true);
			MultiPlayManager.Instance.IsCameraTargetChange = true;
		}
		//UIController.Instance.TimeCountStop ();
		//GameManager.Instance.SceneChange ("Title", 10f);
	}

	IEnumerator CatchFireCoroutine(){
		m_IsCathFire = true;
		//UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, true);
		RpcSetFlameObject (true);
		for (float i = 0; i < m_RestorationTime; i += Time.deltaTime) {
			m_Health -= Time.deltaTime;
			if (m_Health <= 0 && m_IsLive) {
				RpcOnHealthZero ();
				yield break;
			}
			yield return null;
		}
		//yield return new WaitForSeconds (m_RestorationTime);
		RpcSetFlameObject (false);
		//UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, false);
		m_IsCathFire = false;
	}

	IEnumerator InundatedCoroutine(){
		m_IsInundated = true;
		if (isLocalPlayer) {
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, true);
		}
		for (float i = 0; i < m_RestorationTime; i += Time.deltaTime) {
			m_Health -= Time.deltaTime;
			if (m_Health <= 0 && m_IsLive) {
				RpcOnHealthZero ();
				yield break;
			}
			yield return null;
		}
		//yield return new WaitForSeconds (m_RestorationTime);
		if (isLocalPlayer) {
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, false);
		}
		m_IsInundated = false;
	}

	void IsAccelInvoke(){
		if (m_IsLive) {
			m_ShipMovement.IsAccel = true;
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.EngineStop, false);
		}
	}

	void IsRudderInvoke(){
		if(m_IsLive){
			m_ShipMovement.IsRudder = true;
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.RudderStop, false);
		}
	}

	[ClientRpc]
	void RpcSetFlameObject(bool enable){
		for (int i = 0; i < Flames.Length; i++) {
			Flames [i].SetActive (enable);
		}
		if (isLocalPlayer) {
			UIController.Instance.MyStatusAlment (UIController.StatusAlments.CatchFire, enable);
		}
	}

	public void CollisionDamage(Vector3 relativeSpeed){
		float damage = relativeSpeed.magnitude * 20;
		if (m_IsLive) {
			m_Health -= damage;
			if (m_Health <= 0) {
				m_Health = 0;
				RpcOnHealthZero ();
			}
		}
	}

	[ServerCallback]
	void OnCollisionEnter(Collision _other){
		PlayerStatuOnNetwork otherStatus = _other.gameObject.GetComponent<PlayerStatuOnNetwork> ();
		if (otherStatus != null && otherStatus.m_IsLive) {
			Vector3 damage = _other.relativeVelocity;
			_other.gameObject.GetComponent<PlayerStatuOnNetwork> ().CollisionDamage (damage);
		}

	}

	void OnWillRenderObject(){
		if (Camera.current.tag == "MainCamera") {
			m_OnCamera = true;
		}
	}

	void DestroyInvoke(){
		NetworkServer.Destroy (gameObject);
		Destroy (gameObject);
	}

}
