﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class LobbyPlayer : NetworkLobbyPlayer {

	[SerializeField] InputField PlayerNameInput;
	[SerializeField] Button PlayerButton;

	[SerializeField,SyncVar(hook = "UpdatePlayerName")] public string m_PlayerName = "";
	[SerializeField,SyncVar] public Color m_Color;

	public override void OnClientEnterLobby (){
		base.OnClientEnterLobby ();
		ListManager.Instance.Add (this);

		if (isLocalPlayer) {
			PlayerButton.GetComponentInChildren<Text>().text = "Ready";
		} else {
			PlayerButton.GetComponentInChildren<Text> ().text = "---";
		}

		if (m_PlayerName == string.Empty) {
			m_PlayerName = "Player" + ListManager.Instance.transform.childCount;
		}
		if (ListManager.Instance.transform.childCount % 2 == 1) {
			m_Color = Color.green;
		} else {
			m_Color = Color.red;
		}
		PlayerNameInput.text = m_PlayerName;
		PlayerButton.interactable = isLocalPlayer;
		PlayerNameInput.interactable = isLocalPlayer;

	}

	public override void OnStartAuthority (){
		base.OnStartAuthority ();

		PlayerButton.GetComponentInChildren<Text>().text = "Ready";
		PlayerButton.onClick.RemoveAllListeners ();
		PlayerButton.onClick.AddListener (Ready);
		PlayerButton.interactable = true;
		PlayerNameInput.interactable = true;
		PlayerNameInput.onEndEdit.AddListener (NameChange);

	}

	void Update(){

		if (ListManager.Instance.ListCount == 1) {
			PlayerButton.gameObject.SetActive (false);
		} else {
			PlayerButton.gameObject.SetActive (true);
		}

	}

	public override void OnClientReady (bool readyState){

		if (readyState) {
			PlayerButton.GetComponentInChildren<Text>().text = "Waiting";
			PlayerButton.onClick.RemoveAllListeners ();
		} else {
			PlayerButton.GetComponentInChildren<Text>().text = isLocalPlayer ? "Ready" : "----";
			PlayerButton.onClick.RemoveAllListeners ();
			PlayerButton.onClick.AddListener (Ready);
		}

	}
	public void Ready(){
		SendReadyToBeginMessage ();
	}

	public void ReadyCancel(){
		SendNotReadyToBeginMessage ();
	}

	public void UpdatePlayerName(string name){
		PlayerNameInput.text = name;
	}

	public void NameChange(string name){
		CmdNameChange (name);
	}

	[Command]
	public void CmdNameChange(string name){
		m_PlayerName = name;
	}


}
