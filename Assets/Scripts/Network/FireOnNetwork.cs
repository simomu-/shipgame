﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class FireOnNetwork : NetworkBehaviour {

	[HideInInspector] public bool IsAim = true;
	[HideInInspector] public bool IsFire = true;

	[SerializeField] GameObject TorpedoPrefab;
	[SerializeField] GameObject ShellPrefab;
	[SerializeField] GameObject ArpeggioCanon;
	[SerializeField] Transform[] TorpedoTurretTrans;
	[SerializeField] Transform[] GunTurretTrans;
	[SerializeField] Transform[] GunBarrelTrans;
	[SerializeField] float m_Range;
	[SerializeField] float m_ShellVelocity;
	[SerializeField] float m_GunReloadTime;
	[SerializeField] float m_TorpedoReloadTime;
	[SerializeField] float m_RotateSpeed;
	[SerializeField] float m_MinRotateAngle;

	GameObject[] m_TorpedoReticles;
	Transform[] m_ShellCreateTrans;
	FireManager[] m_TorpedoFireManagers;
	FireManager[] m_GunFireManagers;
	Ray m_TurretDirectionRay;
	FireMode m_FireMode = FireMode.Canon;
	bool m_CanGunFire = true;
	bool m_CanTorpedoFire = true;
	bool m_IsArpeggioFire = false;
	//int m_ActiveGunIndex = 0;

	void Awake(){
		m_ShellCreateTrans = new Transform[GunBarrelTrans.Length];
		for (int i = 0; i < GunBarrelTrans.Length; i++) {
			m_ShellCreateTrans[i] = GunBarrelTrans [i].GetChild (0);
		}
		m_GunFireManagers = new FireManager[GunTurretTrans.Length];
		for (int i = 0; i < GunTurretTrans.Length; i++) {
			m_GunFireManagers [i] = GunTurretTrans [i].GetComponent<FireManager> ();
		}
		m_TorpedoReticles = new GameObject[TorpedoTurretTrans.Length];
		m_TorpedoFireManagers = new FireManager[TorpedoTurretTrans.Length];
		for (int i = 0; i < m_TorpedoReticles.Length; i++) {
			m_TorpedoFireManagers [i] = TorpedoTurretTrans [i].GetComponent<FireManager> ();
			m_TorpedoReticles [i] = TorpedoTurretTrans [i].GetChild (0).gameObject;
		}
	}

	void Update(){
		if (!isLocalPlayer) {
			return;
		}

		if (IsAim) {
			TorpedoAim ();
			GunAim ();
		}
		if (m_CanGunFire && m_FireMode == FireMode.Canon && IsFire) {
			GunFire ();
		}

		if (m_CanTorpedoFire && m_FireMode == FireMode.Torpedo && IsFire) {
			TorpedoFire ();
		}
		if (Input.GetButtonDown("Fire1") && !m_IsArpeggioFire && m_FireMode == FireMode.Arpeggio && IsFire) {
			StartCoroutine (ArpeggioFireCoroutine ());
		}

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			m_FireMode = FireMode.Canon;
		}
		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			m_FireMode = FireMode.Torpedo;
		}

		UIController.Instance.MyWeaponType (m_FireMode);

		for (int i = 0; i < GunTurretTrans.Length; i++) {
			m_TurretDirectionRay = new Ray (GunBarrelTrans [i].position, GunTurretTrans [i].forward);
			Vector3 impactPoint = 
				m_TurretDirectionRay.GetPoint (
					Trajectry.TrajectryDistance (m_ShellCreateTrans[i].forward * m_ShellVelocity, m_ShellCreateTrans [i].eulerAngles.x));
			bool isActive = (m_FireMode == FireMode.Canon) && m_CanGunFire && IsFire;
			Color c = (m_GunFireManagers[i].CanFire) ? Color.green : new Color (1f, 0.5f, 0);
			UIController.Instance.SetGunReticlePosition (i,impactPoint,isActive,c);
		}
	}

	void TorpedoAim(){
		float cameraAngle = Camera.main.transform.eulerAngles.y;
		float turretAngle;
		float rotateAngle;
		float absRotateAngle;

		for (int i = 0; i < TorpedoTurretTrans.Length; i++) {
			turretAngle = TorpedoTurretTrans [i].eulerAngles.y;
			rotateAngle = turretAngle - cameraAngle;
			absRotateAngle = Mathf.Abs (rotateAngle);

			if (absRotateAngle <= m_MinRotateAngle) {

				TorpedoTurretTrans[i].Rotate(new Vector3(0, 0, 0));

			}else if ((rotateAngle < 0 && absRotateAngle < 180) || (rotateAngle > 0 && absRotateAngle > 180)) {

				TorpedoTurretTrans[i].Rotate (new Vector3 (0,  m_RotateSpeed * Time.deltaTime, 0));

			} else if ((rotateAngle > 0 && absRotateAngle < 180) || (rotateAngle < 0 && absRotateAngle > 180)) {

				TorpedoTurretTrans[i].Rotate (new Vector3 (0, -1 * m_RotateSpeed * Time.deltaTime, 0));

			}
			m_TorpedoReticles [i].SetActive (m_TorpedoFireManagers [i].CanFire && m_FireMode == FireMode.Torpedo && IsFire);
		}

	}

	void TorpedoFire(){
		if (Input.GetButtonDown ("Fire1")) {
			for (int i = 0; i < m_TorpedoFireManagers.Length; i++) {
				if(m_TorpedoFireManagers[i].CanFire){
					/*Instantiate (TorpedoPrefab, 
						TorpedoTurretTrans [i].position, 
						TorpedoTurretTrans [i].rotation);*/
					CmdTorpedoFire (TorpedoTurretTrans [i].position, TorpedoTurretTrans [i].rotation);
					m_TorpedoFireManagers [i].Fire (m_TorpedoReloadTime);

				}
			}
		}
	}

	[Command]
	void CmdTorpedoFire(Vector3 position,Quaternion rotation){
		GameObject torpedo = Instantiate (TorpedoPrefab, position, rotation) as GameObject;
		float velocity = torpedo.GetComponent<TorpedoOnNetwork> ().Velocity;
		torpedo.GetComponent<Rigidbody> ().velocity = torpedo.transform.forward * velocity;
		NetworkServer.Spawn (torpedo);
	}

	void GunAim(){

		Ray ray = Camera.main.ScreenPointToRay (new Vector3 (Screen.width / 2, Screen.height / 2, 0));

		for (int i = 0; i < GunTurretTrans.Length; i++) {
			Vector3 point = ray.GetPoint (m_Range);
			point.y = GunTurretTrans[i].position.y;
			GunTurretTrans [i].rotation = Quaternion.Slerp (GunTurretTrans [i].rotation,
				Quaternion.LookRotation (point - GunTurretTrans [i].position),
				m_RotateSpeed * Time.deltaTime / 50);
		}

		float cameraAngle = Camera.main.transform.eulerAngles.x;
		float gunAngle;
		float rotateAngle;
		float absRotateAngle;

		for (int i = 0; i < GunBarrelTrans.Length; i++) {
			gunAngle = GunBarrelTrans [i].eulerAngles.z;
			rotateAngle = gunAngle - cameraAngle;
			absRotateAngle = Mathf.Abs (rotateAngle);

			if (absRotateAngle <= 2f) {

				GunBarrelTrans[i].Rotate(new Vector3(0, 0, 0));

			}else if ((rotateAngle < 0 && absRotateAngle < 180) || (rotateAngle > 0 && absRotateAngle > 180)) {

				GunBarrelTrans[i].Rotate (new Vector3 (0, 0, m_RotateSpeed * Time.deltaTime ));

			} else if ((rotateAngle > 90 && absRotateAngle < 180) || (rotateAngle < 0 && absRotateAngle > 180)) {

				GunBarrelTrans[i].Rotate (new Vector3 ( 0, 0, -1 * m_RotateSpeed * Time.deltaTime ));

			}
		}

	}

	void GunFire(){
		if (Input.GetButtonDown ("Fire1")) {
			for (int i = 0; i < m_GunFireManagers.Length; i++) {
				if (m_GunFireManagers [i].CanFire) {
					/*
					GameObject shell = 
						Instantiate (
							ShellPrefab, m_ShellCreateTrans [i].position,
							m_ShellCreateTrans [i].rotation) as GameObject;
					shell.GetComponent<Rigidbody> ().velocity = shell.transform.forward * m_ShellVelocity;*/
					CmdGunFire (m_ShellCreateTrans [i].position, m_ShellCreateTrans [i].rotation);
					m_GunFireManagers [i].Fire (m_GunReloadTime);
				}
			}
		}
	}

	[Command]
	void CmdGunFire(Vector3 potision,Quaternion rotation){
		GameObject shell = Instantiate (ShellPrefab, potision, rotation) as GameObject;
		shell.GetComponent<Rigidbody>().velocity = shell.transform.forward * m_ShellVelocity;
		NetworkServer.Spawn (shell);
	}

	void ResetCanFire(){
		m_CanGunFire = true;
	}

	void ChangeFireMode(FireMode mode){
		m_FireMode = (mode == FireMode.Canon) ? FireMode.Torpedo : FireMode.Torpedo;
	}


	IEnumerator ArpeggioFireCoroutine(){
		m_IsArpeggioFire = true;
		ArpeggioCanon.SetActive (true);
		GameObject child = ArpeggioCanon.transform.GetChild (1).gameObject;
		yield return new WaitForSeconds (1f);
		child.SetActive (true);
		float count = 0;

		while (count <= 10f) {
			count += Time.deltaTime;
			Ray ray = new Ray (ArpeggioCanon.transform.position, ArpeggioCanon.transform.right);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 250f)) {
				Status status = hit.transform.gameObject.GetComponent<Status> ();
				Debug.DrawRay (ray.origin, ray.direction * 300f,Color.red);
				if (status != null) {
					status.Damage (100f, 1, "", WeaponType.Shell);
				}
			}
			yield return null;
		}
		child.SetActive (false);
		ArpeggioCanon.SetActive (false);
		yield return new WaitForSeconds (10f);
		m_IsArpeggioFire = false;
	}

}
