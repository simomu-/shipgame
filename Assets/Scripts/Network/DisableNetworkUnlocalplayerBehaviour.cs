﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class DisableNetworkUnlocalplayerBehaviour : NetworkBehaviour {

    [SerializeField] Behaviour[] behaviors;


	// Use this for initialization
	void Start () {
        if (!isLocalPlayer){
			foreach(var behavior in behaviors){
				behavior.enabled = false;
			}
        }
	
	}

	/*
	void OnApplicationFocus(bool focusStatus){
		if (isLocalPlayer) {
			foreach(var behavior in behaviors){
				behavior.enabled = focusStatus;
			}
		}
	}*/
}
