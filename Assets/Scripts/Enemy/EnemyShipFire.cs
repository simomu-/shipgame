﻿using UnityEngine;
using System.Collections;

public class EnemyShipFire : MonoBehaviour {

	[HideInInspector] public bool IsAim = true;
	[HideInInspector] public bool IsFire = true;

	[SerializeField] GameObject TorpedoPrefab;
	[SerializeField] GameObject ShellPrefab;
	[SerializeField] Transform[] TorpedoTurretTrans;
	[SerializeField] Transform[] GunTurretTrans;
	[SerializeField] Transform[] GunBarrelTrans;
	[SerializeField] float m_SearchRange;
	[SerializeField] float m_ShellVelocity;
	[SerializeField] float m_GunReloadTime;
	[SerializeField] float m_TorpedoReloadTime;
	[SerializeField] float m_RotateSpeed;
	//[SerializeField] float m_MinRotateAngle;
	[Space]
	[SerializeField] float m_ImpactedTime;

	Rigidbody m_TargetRigidbody;
	Rigidbody m_MyRigidbody;

	GameObject[] m_TorpedoReticles;
	Transform[] m_ShellCreateTrans;
	FireManager[] m_TorpedoFireManagers;
	FireManager[] m_GunFireManagers;
	Ray m_TurretDirectionRay;
	FireMode m_FireMode = FireMode.Canon;
	bool m_CanGunFire = true;
	bool m_CanTorpedoFire = true;
	float m_DistanceForTarget;

	//Vector3 shellVelocity;

	void Awake(){
		m_ShellCreateTrans = new Transform[GunBarrelTrans.Length];
		for (int i = 0; i < GunBarrelTrans.Length; i++) {
			m_ShellCreateTrans[i] = GunBarrelTrans [i].GetChild (0);
		}
		m_GunFireManagers = new FireManager[GunTurretTrans.Length];
		for (int i = 0; i < GunTurretTrans.Length; i++) {
			m_GunFireManagers [i] = GunTurretTrans [i].GetComponent<FireManager> ();
		}
		m_TorpedoReticles = new GameObject[TorpedoTurretTrans.Length];
		m_TorpedoFireManagers = new FireManager[TorpedoTurretTrans.Length];
		for (int i = 0; i < m_TorpedoReticles.Length; i++) {
			m_TorpedoFireManagers [i] = TorpedoTurretTrans [i].GetComponent<FireManager> ();
			m_TorpedoReticles [i] = TorpedoTurretTrans [i].GetChild (0).gameObject;
		}
		m_MyRigidbody = GetComponent<Rigidbody> ();
	}

	void Start(){
		m_TargetRigidbody = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
		switch (GameManager.Instance.EnemyAILevel) {
		case 1:
			m_GunReloadTime = 11;
			m_SearchRange = 75;
			break;
		case 2:
			m_GunReloadTime = 9;
			m_SearchRange = 75;
			break;
		case 3:
			m_GunReloadTime = 7;
			m_SearchRange = 100;
			break;
		case 4:
			m_GunReloadTime = 5;
			m_SearchRange = 125;
			break;
		case 5:
			m_GunReloadTime = 3;
			m_SearchRange = 150;
			break;
		}
	}

	void Update(){
		if (m_TargetRigidbody != null) {
			if (Vector3.Distance (transform.position, m_TargetRigidbody.transform.position) <= m_SearchRange) {
				if (IsAim) {
					GunAim ();
				}
				if (IsFire) {
					GunFire ();
				}
			}

			if (Vector3.Distance (transform.position, m_TargetRigidbody.transform.position) <= 150f) {
				if (!GameManager.Instance.IsMusicPlay) {
					GameManager.Instance.MusicPlay ();
					GameManager.Instance.AudioVolume (true);
				}
			}
		}
	}

	void GunAim(){
		for (int i = 0; i < GunTurretTrans.Length; i++) {
			Vector3 referenceVelocity = m_TargetRigidbody.velocity - m_MyRigidbody.velocity;
			Vector3 impactPoint = 
				m_TargetRigidbody.transform.position +
				m_TargetRigidbody.transform.forward * m_TargetRigidbody.velocity.magnitude * m_ImpactedTime;

			GunTurretTrans [i].rotation = Quaternion.Slerp (GunTurretTrans [i].rotation,
				Quaternion.LookRotation (impactPoint - GunTurretTrans [i].position),
				m_RotateSpeed * Time.deltaTime / 50);
		
			m_DistanceForTarget = Vector3.Distance (transform.position, impactPoint);
			/*
		float angle = Trajectry.TrajectryAngle (m_ShellVelocity, distance,m_ImpactedTime);
		Vector3 newEulerAngles = GunBarrelTrans [i].localEulerAngles;
		newEulerAngles.z = angle;
		GunBarrelTrans [i].localEulerAngles = newEulerAngles;
		*/
			/*
		m_ShellVelocity = Trajectry.TrajectrySpeed (distance, 30, m_ImpactedTime);
		m_ShellVelocity = Mathf.Abs (m_ShellVelocity);
		Debug.Log (gameObject.name + " " + m_ShellVelocity);
		*/
			/*
		float angle = Trajectry.TrjectryAngle (m_ShellVelocity, distance, m_ShellHeight);
		//Debug.Log (angle);	
		Vector3 newEulerAngles = GunBarrelTrans [i].eulerAngles;
		newEulerAngles.z = angle * -1;
		GunBarrelTrans [i].eulerAngles = newEulerAngles;
		Debug.Log (Trajectry.TrajectrySpeed (100f, 30f));*/

			Debug.DrawRay (GunTurretTrans [i].position, GunTurretTrans [i].forward * m_DistanceForTarget);

		}
		
	}

	void GunFire(){
		for (int i = 0; i < GunTurretTrans.Length; i++) {
			if (m_GunFireManagers [i].CanFire) {
				GameObject shell = 
					Instantiate (ShellPrefab, 
						GunBarrelTrans [i].position,
						m_ShellCreateTrans[i].rotation) as GameObject; 
				shell.GetComponent<Rigidbody> ().velocity = shell.transform.forward * Trajectry.TrajectryVelocity (m_DistanceForTarget, 30f,out m_ImpactedTime).magnitude;
				m_GunFireManagers [i].Fire (m_GunReloadTime);
			}
		}

	}
		

}
