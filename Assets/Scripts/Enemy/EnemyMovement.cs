﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	[HideInInspector] public bool IsAccel = true;
	[HideInInspector] public bool IsRudder = true;

	[SerializeField] float m_ForwardAccelPower;
	[SerializeField,Range(0,1f)] float m_BackAccelMagnification;
	[SerializeField] float m_RudderTorlque;
	[SerializeField] float m_MinAvoidingTime;
	[SerializeField] float m_MaxAvoidingTime;
	//[SerializeField] Transform m_AccelTransform;
	//[SerializeField] Transform m_MeshTransform;
	Rigidbody m_Rigidbody;
	bool m_IsAvoiding = false;
	float m_RudderControlValue = 0;
	float m_AboidaneRudderValue;

	void Awake(){
		m_Rigidbody = GetComponent<Rigidbody> ();
	}

	void Update(){
		if (IsAccel) {
			Accel ();
		}
		if (IsRudder && m_IsAvoiding) {
			Rudder ();
		}
			
	}

	void Accel(){
		float inputValue = 1f;
		if (inputValue < m_BackAccelMagnification * -1) {
			inputValue = m_BackAccelMagnification * -1;
		}
		m_Rigidbody.AddForce(transform.forward * m_ForwardAccelPower * inputValue * Time.deltaTime,ForceMode.Impulse);
	}

	void Rudder(){
		float speed = m_Rigidbody.velocity.magnitude;
		float inputValue = m_RudderControlValue;
		float rotateValue = m_RudderTorlque * speed * inputValue * Time.deltaTime;
		Quaternion rotate = Quaternion.Euler (0, rotateValue, 0);
		m_Rigidbody.MoveRotation (m_Rigidbody.rotation * rotate);
		//Lean (rotateValue);
	}

	void OnTriggerEnter(Collider _other){
		if (!m_IsAvoiding) {
			m_IsAvoiding = true;
			m_RudderControlValue =(Random.Range (0, 1f) <= 0.5f) ? -1 : 1;
			Invoke ("StopAvoidInvoke", Random.Range (m_MinAvoidingTime, m_MaxAvoidingTime));
		}
	}

	void OnTriggerExit(Collider _other){
		if (m_IsAvoiding) {
			m_IsAvoiding = false;
			CancelInvoke ("StopAvoidInvoke");
		}
	}

	void StopAvoidInvoke(){
		m_IsAvoiding = false;
	}
}
