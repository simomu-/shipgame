﻿using UnityEngine;
using System.Collections;

public class EnemyShipStatus : Status {

	[SerializeField] EnemyMovement m_ShipMovement;
	[SerializeField] EnemyShipFire m_ShipFire;
	[SerializeField] float m_RestorationTime;
	[SerializeField,Range(0,1f)] float m_CatchFireProbability;
	[SerializeField,Range(0,1f)] float m_InundatedProbability;
	[SerializeField] GameObject[] Flames;

	bool m_IsCathFire = false;
	bool m_IsInundated = false;

	public override void Awake (){
		base.Awake ();
		m_ShipName = "AI Enemy Lv" + GameManager.Instance.EnemyAILevel;
	}

	public override void Start (){
		base.Start ();
		GameManager.Instance.EnemySpawnCall ();
	}
		
	public override void Damage (float d, float magnification, string colliderName,WeaponType type){
		switch(colliderName){
		case "Engine":
			m_ShipMovement.IsAccel = false;
			Invoke ("IsAccelInvoke", m_RestorationTime);
			break;
		case "Rudder":
			m_ShipMovement.IsRudder = false;
			Invoke ("IsRudderInvoke", m_RestorationTime);
			break;
		}

		switch (type) {
		case WeaponType.Torpedo:
			if (Random.Range (0, 1f) <= m_InundatedProbability && !m_IsInundated) {
				StartCoroutine (InundatedCoroutine ());
			}
			break;
		case WeaponType.Shell:
			if (Random.Range (0, 1f) <= m_CatchFireProbability && !m_IsCathFire) {
				StartCoroutine (CatchFireCoroutine ());
			}
			break;
		}
		if (m_IsLive) {
			m_Health -= d * magnification;
			if (m_Health <= 0) {
				m_Health = 0;
				OnHealthZero ();
			}
		}
	}

	public override void OnHealthZero (){
		base.OnHealthZero ();
		if (m_Health <= 0) {
			m_ShipMovement.IsAccel = false;
			m_ShipMovement.IsRudder = false;
			m_ShipFire.IsAim = false;
			m_ShipFire.IsFire = false;
			GameManager.Instance.EnemyDestroyedCall ();
		}
	}

	IEnumerator CatchFireCoroutine(){
		m_IsCathFire = true;
		for (int i = 0; i < Flames.Length; i++) {
			Flames [i].SetActive (true);
		}
		for (float i = 0; i < m_RestorationTime; i += Time.deltaTime) {
			m_Health -= Time.deltaTime;
			if (m_Health <= 0 && m_IsLive) {
				OnHealthZero ();
				yield break;
			}
			yield return null;
		}
		for (int i = 0; i < Flames.Length; i++) {
			Flames [i].SetActive (false);
		}
		//yield return new WaitForSeconds (m_RestorationTime);
		m_IsCathFire = false;
	}

	IEnumerator InundatedCoroutine(){
		m_IsInundated = true;
		for (float i = 0; i < m_RestorationTime; i += Time.deltaTime) {
			m_Health -= Time.deltaTime;
			if (m_Health <= 0 && m_IsLive) {
				OnHealthZero ();
				yield break;
			}
			yield return null;
		}
		//yield return new WaitForSeconds (m_RestorationTime);
		m_IsInundated = false;
	}

	void IsAccelInvoke(){
		if (m_IsLive) {
			m_ShipMovement.IsAccel = true;
		}
	}

	void IsRudderInvoke(){
		if (m_IsLive) {
			m_ShipMovement.IsRudder = true;
		}
	}

	void OnTriggerEnter(Collider _other){
		if (_other.transform.root.gameObject.name == "Walls") {
			OnHealthZero ();
		}
	}

}
