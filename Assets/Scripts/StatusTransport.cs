﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class StatusTransport : NetworkBehaviour {

	[SyncVar] public string m_ShipName = "";
	[SyncVar] public Color m_Color;
	[SyncVar(hook = "TransportHealth")] public float m_Health = 100;

	[SerializeField] PlayerShipStatus m_Status;

	public void TransportHealth(float health){
		m_Status.Health = m_Health;
	}

	public void TransportColor(Color color){
		
	}

}
