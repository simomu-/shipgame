﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Cameras;

public class TitleManager : MonoBehaviour {

	[SerializeField] Button[] MainButtons;
	[Space]
	[SerializeField] RectTransform OptionPanel;
	[SerializeField] Slider EnemyAILevelSlider;
	[SerializeField] Slider MouseXSlider;
	[SerializeField] Slider MouseYSlider;
	[SerializeField] Text MouseXText;
	[SerializeField] Text MouseYText;
	[SerializeField] Slider BarrelAngleSlider;
	[SerializeField] Text BarrelAngleText;
	[Space]
	[SerializeField] ScrollRect ArpeggioModeScrollRect;
	[SerializeField] Slider ArpeggioModeSlider;
	[SerializeField] Text ArpeggioModeText;
	[Space]
	[SerializeField] RectTransform LoadPanel;
	[SerializeField] Image LoadImage;
	[Space]
	[SerializeField] RectTransform MessagePanel;
	[SerializeField] Text MessageText;
	[Space]
	[SerializeField] Toggle UseRealtimeToggle;
	[SerializeField] Light SunLight;
	[SerializeField] Light SpotLight;

	Vector3 m_DefaultLightAngle;
	AudioSource[] m_AudioSources;

	void Start(){
		EnemyAILevelSlider.value = GameManager.Instance.EnemyAILevel;
		MouseXSlider.value = (FreeLookCam.MouseX == 1) ? 0 : 1;
		MouseYSlider.value = (FreeLookCam.MouseY == 1) ? 0 : 1;
		BarrelAngleSlider.value = (GameManager.Instance.BarrelAngle == 1) ? 0 : 1;
		ArpeggioModeSlider.value = GameManager.Instance.IsArpeggioMode ? 1 : 0;

		m_DefaultLightAngle = SunLight.transform.localEulerAngles;
		m_AudioSources = GetComponents<AudioSource> ();

	}

	void Update(){
		if (MouseXSlider.value == 0) {
			MouseXText.text = "nomal";
			GameManager.Instance.MouseX = 1;
			FreeLookCam.MouseX = 1;
		} else {
			MouseXText.text = "Reverse";
			GameManager.Instance.MouseX = -1;
			FreeLookCam.MouseX = -1;
		}

		if (MouseYSlider.value == 0) {
			MouseYText.text = "nomal";
			GameManager.Instance.MouseY = 1;
			FreeLookCam.MouseY = 1;
		} else {
			MouseYText.text = "Reverse";
			GameManager.Instance.MouseY = -1;
			FreeLookCam.MouseY = -1;
		}

		if (BarrelAngleSlider.value == 0) {
			BarrelAngleText.text = "Nomal";
			GameManager.Instance.BarrelAngle = 1;
		} else {
			BarrelAngleText.text = "Reverse";
			GameManager.Instance.BarrelAngle = -1;
			MessageDisplay ("悪いな、この機能は未実装なんだ", Color.black,Color.grey);
			BarrelAngleSlider.value = 0;
		}

		if (ArpeggioModeSlider.value == 0) {
			ArpeggioModeText.text = "off";
			GameManager.Instance.IsArpeggioMode = false;
		} else {
			if (!GameManager.Instance.IsArpeggioMode) {
				StartCoroutine (OnClickArrpegioModeCoroutine ());
			}
			ArpeggioModeText.text = "on";
			GameManager.Instance.IsArpeggioMode = true;
		}

		if (UseRealtimeToggle.isOn) {
			DateTime dt = DateTime.Now;
			float sunLightAngle = dt.Hour / 24f;
			Vector3 newAngle = SunLight.transform.localEulerAngles;
			newAngle.x = sunLightAngle * -360 + 270;
			newAngle.y = m_DefaultLightAngle.y;
			newAngle.z = m_DefaultLightAngle.z;
			SunLight.transform.localEulerAngles = newAngle;
			if ((dt.Hour >= 0 && dt.Hour <= 5) || (dt.Hour >= 19 && dt.Hour <= 24)) {
				SunLight.enabled = false;
				SpotLight.enabled = true;
			} else {
				SunLight.enabled = true;
				SpotLight.enabled = false;
			}
		} else {
			SunLight.transform.localEulerAngles = m_DefaultLightAngle;
			SunLight.enabled = true;
			SpotLight.enabled = false;
		}

		GameManager.Instance.EnemyAILevel = (int)EnemyAILevelSlider.value;
	}

	public void OpenFile(string fileName){
		System.Diagnostics.Process.Start (fileName);
	}

	public void SceneChange(string sceneName){
		StartCoroutine (SceneChangeCoroutine (sceneName, 2f));
		LoadPanel.gameObject.SetActive (true);
	}

	public void SceneChangeImmediate(string sceneName){
		SceneManager.LoadScene (sceneName);
	}

	IEnumerator SceneChangeCoroutine(string sceneName,float time){
		float count = 0;
		while (count <= time) {
			count += Time.deltaTime;
			LoadImage.fillAmount = count / time;
			float lateTime = UnityEngine.Random.Range (0, 1f);
			if (lateTime <= 0.15f) {
				yield return new WaitForSeconds (lateTime);
			}
			yield return null;
		}
		SceneManager.LoadScene (sceneName);
	}

	public void OptionButton(){
		OptionPanel.gameObject.SetActive (true);
		for (int i = 0; i < MainButtons.Length; i++) {
			MainButtons [i].interactable = false;	
		}
	}

	public void OptionClose(){
		OptionPanel.gameObject.SetActive (false);
		for (int i = 0; i < MainButtons.Length; i++) {
			MainButtons [i].interactable = true;	
		}
	}

	public void Quit(){
		Application.Quit ();
	}

	public void PlayAudioSources(int index){
		m_AudioSources [index].Play ();
	}

	IEnumerator OnClickArrpegioModeCoroutine(){
		ArpeggioModeScrollRect.verticalNormalizedPosition = 1;
		ArpeggioModeScrollRect.gameObject.SetActive (true);
		while(ArpeggioModeScrollRect.verticalNormalizedPosition >= 0f){
			ArpeggioModeScrollRect.verticalNormalizedPosition -= 0.5f * Time.deltaTime;
			yield return null;
		}
		ArpeggioModeScrollRect.gameObject.SetActive (false);
		MessageDisplay ("Arpeggio Mode is activated!", Color.black, Color.gray);
	}

	void MessageDisplay(string text, Color textColor, Color backColor){
		MessageText.text = text;
		MessageText.color = textColor;
		MessageText.transform.parent.gameObject.GetComponent<Image>().color = backColor;
		MessagePanel.gameObject.SetActive (true);
	}
}
