﻿using System.Collections;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LobbyManager : NetworkLobbyManager {

	public static LobbyManager Instance;

	[SerializeField] Button BackButton;
	[SerializeField] RectTransform SelectPanel;
	[SerializeField] Button CreateButton;
	[SerializeField] Button JoinButton;
	[SerializeField] InputField AddressInput;
	[Space]
	[SerializeField] RectTransform ListPanel;
	[SerializeField] RectTransform ListSubPanel;
	[SerializeField] Text AddressText;

	string m_Address;
	Canvas m_Canvas;
	AudioSource[] m_AudioSources;

	void Awake(){
		if (FindObjectsOfType<LobbyManager> ().Length > 1) {
			Destroy (gameObject);
		}
		string hostname = Dns.GetHostName ();
		IPAddress[] addressList = Dns.GetHostAddresses (hostname);
		foreach (IPAddress address in addressList) {
			m_Address = address.ToString ();
		}
		m_Canvas = GetComponent<Canvas> ();
		m_AudioSources = GetComponents<AudioSource> ();
	}

	void Start(){
		Instance = this;
	}

	void OnLevelWasLoaded(){
		if (SceneManager.GetActiveScene ().name == "LobbyScene") {
			m_Canvas.enabled = true;
		}
		if (SceneManager.GetActiveScene ().name == "Title") {
			m_Canvas.enabled = false;
		}
	}

	public void OnClickCreate(){
		StartHost ();
	}

	public void OnClickJoin(){
		networkAddress = AddressInput.text;
		StartClient ();
	}

	public void BackTitle(){
		StopHost ();
		StopServer ();
		StopClient ();
		SceneManager.LoadScene ("Title");
		//Destroy (gameObject);
	}

	public void PlayAudioSources(int index){
		m_AudioSources [index].Play ();
	}

	public override void OnStartHost (){
		base.OnStartHost ();
		AddressText.text = "your address:" + m_Address;
		SelectPanel.gameObject.SetActive (false);
		ListPanel.gameObject.SetActive (true);
	}
	public override void OnStartClient (NetworkClient lobbyClient){
		base.OnStartClient (lobbyClient);
		AddressText.text = "Host Address:" + m_Address;
		SelectPanel.gameObject.SetActive (false);
		ListPanel.gameObject.SetActive (true);
	}


	public override GameObject OnLobbyServerCreateLobbyPlayer (NetworkConnection conn, short playerControllerId){
		GameObject obj = Instantiate (lobbyPlayerPrefab.gameObject) as GameObject;
		return obj;
	}

	public override void OnLobbyClientSceneChanged (NetworkConnection conn){
		m_Canvas.enabled = false;
		base.OnLobbyClientSceneChanged (conn);
	}

	public override bool OnLobbyServerSceneLoadedForPlayer (GameObject lobbyPlayer, GameObject gamePlayer)
	{
		string name = lobbyPlayer.GetComponent<LobbyPlayer> ().m_PlayerName;
		Color color = lobbyPlayer.GetComponent<LobbyPlayer> ().m_Color;
		gamePlayer.GetComponent<PlayerStatuOnNetwork> ().m_ShipName = name;
		gamePlayer.GetComponent<PlayerStatuOnNetwork> ().m_Color = color;
		return true;

	}


	public override void OnLobbyStopClient ()
	{
		base.OnLobbyStopClient ();
		ListPanel.gameObject.SetActive (false);
		SelectPanel.gameObject.SetActive (true);
	}

	public override void OnLobbyStopHost ()
	{
		base.OnLobbyStopHost ();
		ListPanel.gameObject.SetActive (false);
		SelectPanel.gameObject.SetActive (true);
	}
		
	/*
	public override void OnServerError (NetworkConnection conn, int errorCode)
	{
		base.OnServerError (conn, errorCode);
		m_Canvas.enabled = true;
		ListPanel.gameObject.SetActive (false);
		SelectPanel.gameObject.SetActive (true);
	}

	public override void OnClientError (NetworkConnection conn, int errorCode)
	{
		base.OnClientError (conn, errorCode);
		m_Canvas.enabled = true;
		ListPanel.gameObject.SetActive (false);
		SelectPanel.gameObject.SetActive (true);
	}*/
}
