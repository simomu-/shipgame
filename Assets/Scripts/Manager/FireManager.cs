﻿	using UnityEngine;
using System.Collections;

public class FireManager: MonoBehaviour {
	
	[SerializeField] float[] BeginAngles;
	[SerializeField] float[] EndAngles;
	bool m_CanGunFire = true;

	public bool CanFire{
		get{
			m_CanGunFire = false;
			for (int i = 0; i < BeginAngles.Length; i++) {
				if (transform.localEulerAngles.y >= BeginAngles[i] && transform.localEulerAngles.y <= EndAngles[i] && !IsInvoking("Reload")) {
					m_CanGunFire = true;
				}
			}
			return m_CanGunFire;
		}
	}

	public void Fire(float reloadTime){
		m_CanGunFire = false;
		AudioSource audio = GetComponent<AudioSource> ();
		if (audio != null) {
			audio.Play ();
		}
		Invoke ("Reload", reloadTime);
	}

	void Reload(){
		m_CanGunFire = true;
	}

}
