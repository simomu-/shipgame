﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;

public class GameManager : SingletonMonoBehaviour<GameManager> {

	[HideInInspector] public int EnemyAILevel = 1;
	[HideInInspector] public int MouseX = 1;
	[HideInInspector] public int MouseY = 1;
	[HideInInspector] public int BarrelAngle = 1;
	[HideInInspector] public int DestroyedEnemy = 0;
	[HideInInspector] public bool IsArpeggioMode = false;
	[HideInInspector] public bool IsMusicPlay = false;

	[SerializeField] GameObject m_EnemyPrefab;
	[SerializeField] float m_EnemySpawnInterval;
	[SerializeField] int m_MaxEnemy;

	int m_SpawnedEnemyCount = 0;
	Transform[] EnemySpawnTrans;
	AudioSource[] m_AudioSource;

	void Start(){
		switch (Application.platform) {
		case RuntimePlatform.WindowsPlayer:
			break;
		case RuntimePlatform.OSXPlayer:
			break;
		}
		Analytics.CustomEvent ("OS", new Dictionary<string,object>{ { "OS",Application.platform.ToString () } });
		m_AudioSource = GetComponents<AudioSource> ();
	}

	void OnLevelWasLoaded(){
		if (SceneManager.GetActiveScene ().name == "SinglePlayGame") {
			Transform spawnTrans = GameObject.Find ("EnemySpawnPositions").transform;
			int i = 0;
			EnemySpawnTrans = new Transform[spawnTrans.childCount];
			foreach (Transform child in spawnTrans) {
				EnemySpawnTrans [i] = child;
				i++;
			}
			m_SpawnedEnemyCount = 0;
			DestroyedEnemy = 0;
			InvokeRepeating ("EnemySpawnInvoke", 0, m_EnemySpawnInterval);
		}
		if (SceneManager.GetActiveScene ().name == "Title") {
			StopCoroutine (MusicPlayCoroutine ());
			IsMusicPlay = false;
			for (int i = 0; i < m_AudioSource.Length; i++) {
				m_AudioSource [i].Stop ();
			}
		}
	}

	void EnemySpawnInvoke(){
		if (m_SpawnedEnemyCount < m_MaxEnemy) {
			int index = Random.Range (0, EnemySpawnTrans.Length);
			Instantiate (m_EnemyPrefab, EnemySpawnTrans [index].position, EnemySpawnTrans [index].rotation);
		}
	}

	public void EnemySpawnCall(){
		m_SpawnedEnemyCount++;
	}

	public void EnemyDestroyedCall(){
		m_SpawnedEnemyCount--;
		DestroyedEnemy++;
		UIController.Instance.SetDestroyEnemyCountText (DestroyedEnemy);
	}

	public void FinishSinglePlayGame(int destroyCount,float serviveTime){
		Analytics.CustomEvent ("SinglePlayGame", new Dictionary<string,object> {
			{ "DestroyCount",destroyCount },
			{ "EnemyAILevel",GameManager.Instance.EnemyAILevel},
			{ "ServiveTime",serviveTime },
			{ "UseArpeggioMode",IsArpeggioMode}
		});
	}

	public void FinishMultiPlayGame(string WinnerTeam){
		Analytics.CustomEvent ("MultiPlayGame", new Dictionary<string,object> {
			{ "IsGreenTeamWin",WinnerTeam }
		});
	}

	public void SceneChange(string sceneName, float time){
		StartCoroutine (SceneChangeCoroutine (sceneName, time));
	}

	IEnumerator SceneChangeCoroutine(string sceneName, float time){
		yield return new WaitForSeconds (time);
		SceneManager.LoadScene (sceneName);
	}

	public void MusicPlay(){
		StartCoroutine (MusicPlayCoroutine ());	
	}

	IEnumerator MusicPlayCoroutine(){
		IsMusicPlay = true;
		while(IsMusicPlay){
			int index = (int)Random.Range (0, m_AudioSource.Length);
			m_AudioSource [index].Play ();
			yield return new WaitForSeconds (m_AudioSource [index].clip.length + 1f);
		}
	}
	public void AudioVolume(bool isVolumeUp){
		StartCoroutine (AuidoVolumeCoroutine (isVolumeUp));
	}
	IEnumerator AuidoVolumeCoroutine(bool isVolumeUp){
		float time;
		if (isVolumeUp) {
			time = 0;
			while(time <= 5f){
				for (int i = 0; i < m_AudioSource.Length; i++) {
					m_AudioSource [i].volume = time / 5f * 0.2f;
				}
				time += Time.deltaTime;
				yield return null;
			}
		} else {
			time = 3f;
			while(time >= 0){
				for (int i = 0; i < m_AudioSource.Length; i++) {
					m_AudioSource [i].volume = time / 3f * 0.2f;
				}
				time -= Time.deltaTime;
				yield return null;
			}
			for (int i = 0; i < m_AudioSource.Length; i++) {
				m_AudioSource [i].volume = 0;
			}
		}
	}

}
