﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Cameras;

public class MultiPlayManager : MonoBehaviour {

	public static MultiPlayManager Instance;

	public bool IsCameraTargetChange = false;
	public bool IsFinish = false;

	List<PlayerStatuOnNetwork> PlayerList = new List<PlayerStatuOnNetwork>();
	int m_CameraTargetIntex = 0;
	int m_GreenCount = 0;
	int m_RedCount = 0;
	bool m_IsGame = true;
	PlayerStatuOnNetwork m_CameraTargetPlayer;

	void Awake(){
		Instance = this;
	}

	void Update(){
		if (IsCameraTargetChange && Input.GetButtonDown ("Fire1")) {
			m_CameraTargetIntex = ChangeCameraTarget ( m_CameraTargetIntex);
		}

		if ((m_GreenCount <= 0 || m_RedCount <= 0) && m_IsGame && IsFinish) {
			JudgeGreenTeamWin ();
		}

	}

	public void Add(PlayerStatuOnNetwork player){
		PlayerList.Add (player);
		UIController.Instance.AddMultiPlayer (player.m_Color);
		if (player.m_Color == Color.green) {
			m_GreenCount++;
		} else {
			m_RedCount++;
		}
	}

	public void Remove(PlayerStatuOnNetwork player){
		UIController.Instance.RemoveMultiPlayer (player.m_Color);
		PlayerList.Remove (player);
		if (player.m_Color == Color.green) {
			m_GreenCount--;
		} else {
			m_RedCount--;
		}
	}

	int ChangeCameraTarget(int index){
		int newIndex = index;
		//PlayerList[newIndex].gameObject.tag = "Untagged";
		newIndex++;
		if (newIndex >= PlayerList.Count) {
			newIndex = 0;
		}
		//PlayerList[newIndex].gameObject.tag = "Player";
		Camera.main.transform.root.gameObject.GetComponent<FreeLookCam>().Target = PlayerList[newIndex].transform;
		return newIndex;
	}

	public void JudgeGreenTeamWin(){

		if (m_GreenCount == m_RedCount) {
			m_IsGame = false;
			UIController.Instance.SetMessageText ("Draw", new Color(1f, 0.5f, 0), true);
			GameManager.Instance.FinishMultiPlayGame ("Draw");
			LobbyManager.Instance.Invoke ("StopHost", 10f);
		} else if (m_GreenCount > m_RedCount) {
			m_IsGame = false;
			UIController.Instance.SetMessageText ("Green team Win", Color.green, true);
			GameManager.Instance.FinishMultiPlayGame ("Green");
			LobbyManager.Instance.Invoke ("StopHost", 10f);
		} else {
			m_IsGame = false;
			UIController.Instance.SetMessageText ("Red team win", Color.red, true);
			GameManager.Instance.FinishMultiPlayGame ("Red");
			LobbyManager.Instance.Invoke ("StopHost", 10f);
		}

	}

}
