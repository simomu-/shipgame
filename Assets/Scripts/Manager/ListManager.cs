﻿using UnityEngine;
using System.Collections;

public class ListManager : MonoBehaviour {

	public static ListManager Instance;	

	int m_ListCount;
	public int ListCount{
		get{return m_ListCount;}
	}


	void Awake(){
		Instance = this;
	}

	void Update(){
		m_ListCount = transform.childCount;
		LobbyManager.Instance.minPlayers = m_ListCount;
	}

	public void Add(LobbyPlayer player){
		player.transform.SetParent (transform, false);
	}


}