﻿using UnityEngine;
using System.Collections;

public class Status : MonoBehaviour {

	[SerializeField] protected float m_Health = 100f;
	[SerializeField] GameObject m_PositionUI;
	[SerializeField] protected string m_ShipName;
	[SerializeField] Color m_Color = Color.green;
	[SerializeField] GameObject m_ExplosionPrefab;
	[SerializeField] AudioSource[] m_ExplosionAudio;
	protected float	NormalHealth{
		get{
			return m_Health / 100f;
		}
	}
	public float Health{
		set{
			m_Health = value;
			if (m_Health <= 0 && m_IsLive) {
				OnHealthZero ();
			}
		}
		get{
			return m_Health;
		}
	}
	protected bool m_IsLive = true;
	protected bool m_OnZeroHealthHappen = false;

	protected bool m_OnCamera = false;

	public virtual void Awake(){

	}

	public virtual void Start(){
		SetMyColor ();
		UIController.Instance.AddShip (transform,m_ShipName,m_Color);
	}

	public virtual void Update(){
		FixTransform ();

		if (m_IsLive) {
			float distance = Vector3.Distance (Camera.main.transform.position, transform.position);
			if (m_OnCamera && distance <= 100f) {
				UIController.Instance.UpdateHealth (transform, NormalHealth, true);
			} else {
				UIController.Instance.UpdateHealth (transform, NormalHealth, false);
			}
			m_OnCamera = false;
		}
	}

	public void SetMyColor(){
		m_PositionUI.GetComponent<SpriteRenderer> ().color = m_Color;
	}

	public void FixTransform(){
		if (m_IsLive) {
			Vector3 newPosition = transform.position;
			newPosition.y = 0;
			transform.position = newPosition;
			/*
			Vector3 newAngle = transform.localEulerAngles;
			newAngle.x = 0;
			newAngle.z = 0;
			transform.localEulerAngles = newAngle;
			*/
		}
	}

	public virtual void Damage(float d,float magnification,string colliderName,WeaponType type){
	}

	public virtual void OnHealthZero(){
		m_IsLive = false;
		m_PositionUI.SetActive (false);
		Rigidbody rigidbody = GetComponent<Rigidbody> ();
		rigidbody.constraints = RigidbodyConstraints.None;
		rigidbody.angularDrag = 10f;
		rigidbody.velocity -= transform.up * 0.5f;
		UIController.Instance.RemoveShip (transform);
		Instantiate (m_ExplosionPrefab,transform.position,Quaternion.identity);
		m_ExplosionAudio [(int)Random.Range (0, m_ExplosionAudio.Length)].Play ();
		Destroy (gameObject, 10f);
	}

	public void CollisionDamage(Vector3 relativeSpeed){
		float damage = relativeSpeed.magnitude * 20;
		if (m_IsLive) {
			m_Health -= damage;
			if (m_Health <= 0) {
				m_Health = 0;
				OnHealthZero ();
			}
		}
	}

	void OnCollisionEnter(Collision _other){
		Status otherStatus = _other.gameObject.GetComponent<Status> ();
		if (otherStatus != null && otherStatus.m_IsLive) {
			Vector3 damage = _other.relativeVelocity;
			_other.gameObject.GetComponent<Status> ().CollisionDamage (damage);
		}

	}

	void OnWillRenderObject(){
		if (Camera.current.tag == "MainCamera") {
			m_OnCamera = true;
		}
	}
}	
