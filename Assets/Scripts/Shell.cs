﻿using UnityEngine;
using System.Collections;

public class Shell : MonoBehaviour {

	[SerializeField] float m_OffensivePower;
	[SerializeField,Range(0,1f)] float m_PowerBand;
	[SerializeField] float m_LifeTime;
	[SerializeField] Collider m_Collider;

	float m_ColliderActiveTime = 0.5f;

	void Awake(){
		Destroy (gameObject, m_LifeTime);
		Invoke ("SetColliderActibeInvoke", m_ColliderActiveTime);
	}

	void OnCollisionEnter(Collision _other){
		try{
			string name = _other.collider.name;
			float magnification = Random.Range(1 - m_PowerBand,1 + m_PowerBand);
			_other.gameObject.GetComponent<Status>().Damage(m_OffensivePower,magnification,name,WeaponType.Shell);
			Destroy (gameObject);
		}catch(System.Exception e){
			Debug.Log ("No Content Collision Collision Object Name :" + _other.gameObject.name + " " + e);
		}
	}

	void SetColliderActibeInvoke(){
		m_Collider.enabled = true;
	}
}
