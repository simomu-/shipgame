﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UIController : MonoBehaviour {

	public enum StatusAlments{
		CatchFire = 0,Inuntaded,EngineStop,RudderStop
	}

	public static UIController Instance;
	[SerializeField] Transform[] GunReticleTrans;
	[SerializeField] Transform TorpedoUI;
	[SerializeField] Transform OtherHealthUI;
	[SerializeField] Image MyHealthUI;
	[SerializeField] Image[] ShipStatusUI;
	[SerializeField] Text TimeText;
	[SerializeField] Text MessageText;
	[SerializeField] Text DestroyedEnemyCountText;
	[SerializeField] Text WeaponTypeText;
	[SerializeField] float m_StartTime = 301f;
	[Space]
	[SerializeField] Transform GreenListUI;
	[SerializeField] Transform RedListUI;
	[SerializeField] Image ListImage;
	[Space]
	[SerializeField] RectTransform EscPanel;
	[Space]
	[SerializeField]RectTransform LoadPanel;

	List<Transform> ShipList = new List<Transform> ();
	List<Transform> TorpedoPositionList = new List<Transform>();
	List<Transform> TorpedoUIList = new List<Transform>();
	List<Transform> HealthUIList = new List<Transform>();

	float m_CurrentTime = 0;

	public float ServiveTime{
		get{
			return m_StartTime - m_CurrentTime;
		}
	}

	bool m_IsTimeCount = true;

	void Awake(){
		Instance = this;
	}

	void Start(){
		m_CurrentTime = m_StartTime;
	}

	void Update(){
		if (m_IsTimeCount) {
			m_CurrentTime -= Time.deltaTime;
			TimeText.text = (int)m_CurrentTime / 60 + ":" + (int)m_CurrentTime % 60; 
			int millisecond = (int)Mathf.Floor((m_CurrentTime - Mathf.Floor(m_CurrentTime)) * 100)	;
			TimeText.text = String.Format ("{0:00}:{1:00}:{2:00}", (int)m_CurrentTime / 60, (int)m_CurrentTime % 60,millisecond);
			if (m_CurrentTime <= 0) {
				SetMessageText ("Finish!", Color.green, true);
				if (LobbyManager.Instance != null && LobbyManager.Instance.isNetworkActive) {
					MultiPlayManager.Instance.JudgeGreenTeamWin ();
				} else {
					GameManager.Instance.FinishSinglePlayGame (GameManager.Instance.DestroyedEnemy, m_StartTime - m_CurrentTime);
					GameManager.Instance.SceneChange ("Title", 5f);
					GameManager.Instance.AudioVolume (false);
				}
				m_IsTimeCount = false;
			}
		}
	}

	public void AddShip(Transform trans,string name,Color color){
		ShipList.Add (trans);
		Transform ui = Instantiate (OtherHealthUI) as Transform;
		ui.SetParent (transform, false);
		ui.GetChild (0).GetComponent<Text> ().text = name;
		ui.GetChild (0).GetComponent<Text> ().color = color;
		HealthUIList.Add (ui);
	}

	public void RemoveShip(Transform trans){
		int index = ShipList.IndexOf (trans);
		Destroy (HealthUIList [index].gameObject);
		HealthUIList.Remove (HealthUIList [index]);
		ShipList.Remove (trans);
	}

	public void SetGunReticlePosition(int index, Vector3 position,bool enable,Color c){
		GunReticleTrans [index].GetComponent<Image> ().color = c;
		GunReticleTrans [index].position = Camera.main.WorldToScreenPoint (position);
		GunReticleTrans [index].gameObject.SetActive (enable);
	}

	public void AddTorpedo(Transform trans){
		TorpedoPositionList.Add (trans);
		Transform ui = Instantiate (TorpedoUI) as Transform;
		ui.SetParent (transform, false);
		ui.gameObject.SetActive (true);
		TorpedoUIList.Add (ui);
	}

	public void TorpedoUIUpdate(Transform trans,bool enable){
		int index = TorpedoPositionList.IndexOf (trans);
		TorpedoUIList[index].position = Camera.main.WorldToScreenPoint (trans.position);
		TorpedoUIList [index].gameObject.SetActive (enable);
	}

	public void RemoveTorpedo(Transform trans){
		int index = TorpedoPositionList.IndexOf (trans);
		Destroy (TorpedoUIList [index].gameObject);
		TorpedoUIList.Remove (TorpedoUIList [index]);
		TorpedoPositionList.Remove (trans);
	}
	/*
	public void AddHealth(Transform trans){
		Transform ui = Instantiate (OtherHealthUI) as Transform;
		ui.SetParent (transform, false);
		HealthUIList.Add (ui);
	}
	*/

	public void UpdateHealth(Transform trans,float healthValue, bool enable){
		int index = ShipList.IndexOf (trans);
		HealthUIList [index].position = Camera.main.WorldToScreenPoint (trans.position);
		HealthUIList [index].gameObject.GetComponent<Slider> ().value = healthValue;
		HealthUIList [index].gameObject.SetActive (enable);
	}
	/*
	public void RemoveHealth(Transform trans){
		int index = ShipList.IndexOf (trans);
		Destroy (HealthUIList [index].gameObject);
		HealthUIList.Remove (HealthUIList [index]);
	}
	*/

	public void UpdateMyHealth(float healthvalue){
		MyHealthUI.fillAmount = healthvalue;
	}

	public void SetMessageText(string text, Color color, bool enable){
		MessageText.text = text;
		MessageText.color = color;
		MessageText.gameObject.SetActive (enable);
	}

	public void TimeCountStop(){
		m_IsTimeCount = false;
	}

	public void SetDestroyEnemyCountText(int num){
		DestroyedEnemyCountText.text = String.Format ("Destroyed:{0:00}", num);
	}

	public void MyStatusAlment(StatusAlments status,bool enable){
		ShipStatusUI [(int)status].gameObject.SetActive (enable);
	}

	public void MyWeaponType(FireMode type){
		WeaponTypeText.text = "weapon:" + type.ToString();
	}

	public void AddMultiPlayer(Color c){
		GameObject element = Instantiate (ListImage.gameObject) as GameObject;
		if (c == Color.green) {
			element.transform.SetParent (GreenListUI, false);
			element.SetActive (true);
		} else {
			element.transform.SetParent (RedListUI, false);
			element.SetActive (true);
		}
		element.gameObject.GetComponent<Image> ().color = c;
	}

	public void RemoveMultiPlayer(Color c){
		if (c == Color.green) {
			Destroy(GreenListUI.GetChild (1).gameObject);
		} else {
			Destroy (RedListUI.GetChild (1).gameObject);
		}
	}

	public void SetActiveEscPanel(bool enable){
		EscPanel.gameObject.SetActive (enable);
		Cursor.lockState = enable ? CursorLockMode.None : CursorLockMode.Locked;
		Cursor.visible = enable;
	}

	public void BackTitle(){
		//GameManager.Instance.FinishSinglePlayGame (GameManager.Instance.DestroyedEnemy, UIController.Instance.ServiveTime);
		GameManager.Instance.SceneChange ("Title", 0.1f);
	}

	public void Restart(){
		GameManager.Instance.FinishSinglePlayGame (GameManager.Instance.DestroyedEnemy, UIController.Instance.ServiveTime);
		StartCoroutine (RestartCoroutine (0.5f));
	}

	IEnumerator RestartCoroutine(float time){
		EscPanel.gameObject.SetActive (false);
		LoadPanel.gameObject.SetActive (true);
		Image LoadImage = LoadPanel.GetChild (0).GetComponent<Image> ();
		float count = 0;
		while (count <= time) {
			count += Time.deltaTime;
			LoadImage.fillAmount = count / time;
			float lateTime = UnityEngine.Random.Range (0, 1);
			if (lateTime <= 0.15f) {
				yield return new WaitForSeconds (lateTime);
			}
			yield return null;
		}
		UnityEngine.SceneManagement.SceneManager.LoadScene("SinglePlayGame");
	}

}
