﻿using UnityEngine;
using System.Collections;

public class Trajectry{

	public static float TrajectryDistance(Vector3 velocity, float eulerAngle){
		float distance = 
			2 * velocity.sqrMagnitude *
			Mathf.Sin (eulerAngle * Mathf.PI / 180) *
			Mathf.Cos (eulerAngle * Mathf.PI / 180) / Physics.gravity.y;
		return distance;
	}
	/*
	public static float TrajectryDistance(Vector3 velocity, float eulerAngle,out float time){

	}*/
	/*
	public static float TrajectryAngle(float velocity, float distance, float time){
		float angle = Mathf.Acos (distance / (2 * velocity * time)) * 180f / Mathf.PI;
		return angle;
	}
	*/

	public static float TrjectryAngle(float velocity, float distance, float height){
		float angleHS = Mathf.Atan (height / distance);
		float theta = 
			(Mathf.Asin ((Physics.gravity.y * distance * distance / (velocity * velocity) + height) /
			Mathf.Sqrt (distance * distance + height * height)) + angleHS) / 2;
		return theta;
	}

	public static float TrajectrySpeed(float distance, float angle){
		float speed = Mathf.Sqrt(distance * Physics.gravity.y * -1 / Mathf.Sin (2 * angle * Mathf.Deg2Rad));
		return speed;
	}

	public static Vector3 TrajectryVelocity(float distance ,float angle){
		float projectileVelocity = distance * Physics.gravity.y * -1 / Mathf.Sin (2 * angle * Mathf.Deg2Rad);
		Vector3 resultVelocity = new Vector3 (0, 0, 0);
		resultVelocity.z = Mathf.Sqrt (projectileVelocity) * Mathf.Cos (angle * Mathf.Deg2Rad);
		resultVelocity.y = Mathf.Sqrt (projectileVelocity) * Mathf.Sin (angle * Mathf.Deg2Rad);
		return resultVelocity;
	}

	public static Vector3 TrajectryVelocity(float distance ,float angle, out float time){
		float projectileVelocity = distance * Physics.gravity.y * -1 / Mathf.Sin (2 * angle * Mathf.Deg2Rad);
		Vector3 resultVelocity = new Vector3 (0, 0, 0);
		resultVelocity.z = Mathf.Sqrt (projectileVelocity) * Mathf.Cos (angle * Mathf.Deg2Rad);
		resultVelocity.y = Mathf.Sqrt (projectileVelocity) * Mathf.Sin (angle * Mathf.Deg2Rad);
		time = distance / (resultVelocity.magnitude * Mathf.Cos (angle * Mathf.Deg2Rad));
		return resultVelocity;
	}

}
