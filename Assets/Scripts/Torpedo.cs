﻿using UnityEngine;
using System.Collections;

public class Torpedo : MonoBehaviour {

	[SerializeField] float m_Velocity;
	[SerializeField] float m_OffensivePower;
	[SerializeField,Range(0,1f)] float m_PowerBand;
	[SerializeField] Collider m_Collider;
	[SerializeField] TrailRenderer m_TrailRenderer;
	[SerializeField] float m_LifeTime;
	Rigidbody m_Rigidbody;
	bool m_OnCamera;

	void Awake(){
		m_Rigidbody = GetComponent<Rigidbody> ();
		m_Rigidbody.velocity = transform.forward * m_Velocity;
		UIController.Instance.AddTorpedo (transform);
		Destroy (gameObject, m_LifeTime);
	}

	void Update(){
		if (transform.position.y <= 0 && m_Rigidbody.useGravity) {
			m_Rigidbody.useGravity = false;
			Vector3 angle = transform.eulerAngles;
			angle.z = 0;
			transform.eulerAngles = angle;
			Vector3 position = transform.position;
			position.y = 0;
			transform.position = position;
			m_Rigidbody.constraints = RigidbodyConstraints.FreezePositionY;
			m_Collider.enabled = true;
			m_TrailRenderer.enabled = true;
		}
			
		float distance = Vector3.Distance (Camera.main.transform.position, transform.position);
		if (m_OnCamera && distance <= 100f) {
			UIController.Instance.TorpedoUIUpdate (transform, true);
		} else {
			Debug.Log ("Disable");
			UIController.Instance.TorpedoUIUpdate (transform, false);
		}
		Debug.Log (m_OnCamera);
		m_OnCamera = false;
	}

	void OnDestroy(){
		UIController.Instance.RemoveTorpedo (transform);
	}

	void OnCollisionEnter(Collision _other){

		try{
			string name = _other.collider.name;
			float magnification = Random.Range(1 - m_PowerBand,1 + m_PowerBand);
			_other.gameObject.GetComponent<Status>().Damage(m_OffensivePower,magnification,name,WeaponType.Torpedo);
		}catch(System.Exception e){
			Debug.Log ("No Content Collision" + e);
		}
		Destroy (gameObject);
	}

	void OnWillRenderObject(){
		if (Camera.current.tag == "MainCamera") {
			m_OnCamera = true;
		}
	}

}
